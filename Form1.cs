﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FMRename
{
    public partial class Form1 : Form
    {
        private int totalNum = 0;//总文件数
        private int totalJosnNum = 0;//总文件数
        private int handledNum = 0;//已处理文件数

        #region 控件代理

        delegate void delegateTSSL(string s, System.Windows.Forms.ToolStripStatusLabel tssl);//创建一个代理
        /// <summary>
        /// Label 重载
        /// </summary>
        /// <param name="s"></param>
        /// <param name="lab"></param>
        private void SetTSSLText(string s, System.Windows.Forms.ToolStripStatusLabel tssl)
        {

            if (!InvokeRequired)//判断是否需要进行唤醒的请求
            {
                tssl.Text = s;
                tssl.Image = null;
            }
            else
            {
                delegateTSSL tssl1 = new delegateTSSL(SetTSSLText);
                Invoke(tssl1, new object[] { s, tssl });//执行唤醒操作 
            }
        }
        #endregion

        public Form1()
        {
            InitializeComponent();
            tsslabState.Text = DateTime.Now.ToLongDateString();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            using (CountdownEvent latch = new CountdownEvent(1))
            {

                Thread t = new Thread(new ThreadStart(() =>
                {
                    StartWork(latch);
                }));
                t.Start();

                latch.Wait();
                latch.Dispose();

                tsslabState.Image = null;
                tsslabState.Text = "查找到" + totalJosnNum + "个专辑文件，已处理音频文件 " + handledNum + "/" + totalNum + " 个，处理完毕！";
            }
        }

        private void StartWork(CountdownEvent latch)
        {
            try
            {
                totalJosnNum = 0;
                totalNum = 0;
                handledNum = 0;
                tsslabState.Text = "开始中，请稍等...";
                tsslabState.Image = global::FMRename.Properties.Resources.runNow;

                List<string> jsonFiles = GetAlbums(this.tbFloder.Text);
                totalJosnNum = jsonFiles.Count;
                if (totalJosnNum == 0)
                {
                    tsslabState.Text = "没有查找到专辑文件！";
                    return;
                }
                tsslabState.Text = "查找到" + totalJosnNum + "个专辑文件！";

                Hashtable files = new Hashtable();
                foreach (var jsonFile in jsonFiles)
                {
                    List<FileInfo> fileInfos = new List<FileInfo>();
                    // 读取json文件，转换成string类型，再反序列化为类对象
                    using (StreamReader file = File.OpenText(jsonFile))
                    {
                        using (JsonTextReader reader = new JsonTextReader(file))
                        {
                            JToken token = JToken.ReadFrom(reader);
                            string json = token.ToString();
                            fileInfos = JsonConvert.DeserializeObject<List<FileInfo>>(json);
                        }
                    }
                    totalNum += fileInfos.Count;
                    files.Add(jsonFile, fileInfos);

                    tsslabState.Text = "查找到" + totalJosnNum + "个专辑文件，待重命名音频文件" + totalNum + "个！";
                }

                foreach (DictionaryEntry o in files)
                {
                    RenameFiles(o.Key.ToString(), (List<FileInfo>)o.Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("未知异常：" + ex.Message);
                LogHelper.SaveErrorLog("未知异常", ex);
            }
            finally
            {
                latch.Signal(); // 执行完毕
            }
        }

        /// <summary>
        /// 重命名 -- 整个文件夹
        /// </summary>
        /// <param name="jsonFile"></param>
        /// <param name="fileInfo"></param>
        private void RenameFiles(String jsonFile, List<FileInfo> fileInfo)
        {
            if (jsonFile.Length == 0 || fileInfo == null || fileInfo.Count == 0)
            {
                return;
            }
            String folder = jsonFile.Replace("list.json", "");//音频文件目录 
            // 获取目录中的音频文件
            System.IO.FileInfo[] files = new DirectoryInfo(folder).GetFiles();

            // 根据json中的id信息找到对应的文件，然后用title和.mp3组成新的文件名，最后用MoveTo方法进行重命名
            foreach (var file in files)
            {
                string[] sArray = file.Name.Split('.'); //获取文件不带扩展名的文件名
                foreach (var item in fileInfo)
                {
                    if (sArray[0] == item.id)
                    {
                        try
                        {
                            string destPath = Path.Combine(folder, item.title + Path.GetExtension(file.Name).ToLower()); //组合成新的文件名称和原始路径
                            file.MoveTo(destPath); // 文件重命名

                            handledNum++;
                            tsslabState.Text = "查找到" + totalJosnNum + "个专辑文件，已处理音频文件 " + handledNum + "/" + totalNum + " 个！";
                        }
                        catch (Exception ex)
                        {
                            LogHelper.SaveErrorLog("未知异常", ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 查找专辑文件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<string> GetAlbums(string path)
        {
            List<string> list = new List<string>();
            if (Directory.Exists(path))
            {
                list.Clear();
                foreach (string text1 in Directory.GetFileSystemEntries(path))
                {
                    string fullPath = Path.GetFullPath(text1);
                    string fileName = Path.GetFileName(text1);

                    if (fileName.EndsWith("list.json"))
                    {
                        list.Add(path + "\\" + fileName);
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// 选择文件夹
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBrowserFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = "d:\\";
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.tbFloder.Text = dialog.SelectedPath;
            }
        }
    }
}
